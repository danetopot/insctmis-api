﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;


namespace INSCTMIS.Api.Models
{

    public class Region : CreatedOnByFields
    {
        [Required]
        public int RegionID { get; set; }

        [Required]
        public string RegionCode { get; set; }

        [Required]
        public string RegionName { get; set; }
    }

    public class Kebele : CreatedOnByFields
    {
        [Required]
        public int KebeleID { get; set; }

        [Required]
        public int WoredaID { get; set; }

        public Woreda Woreda { get; set; }

        [Required]
        public string KebeleCode { get; set; }

        [Required]
        public string KebeleName { get; set; }
    }

    public class Woreda : CreatedOnByFields
    {
        [Required]
        public int WoredaID { get; set; }

        [Required]
        public int RegionID { get; set; }

        public Region Region { get; set; }

        [Required]
        public string WoredaCode { get; set; }

        [Required]
        public string WoredaName { get; set; }
    }

    public class ServiceProvider
    {
        [Required]
        public int ServiceProviderID { get; set; }

        [Required]
        public string ServiceProviderName { get; set; }
    }

    public class IntegratedServices
    {
        [Key]
        [Required]
        public int ServiceID { get; set; }

        [Required]
        public int ServiceProviderID { get; set; }

        public ServiceProvider ServiceProvider { get; set; }

        [Required]
        public string ServiceName { get; set; }
    }

    public class FiscalYears : ReplicatedOnByFields
    {
        [Key]
        [Required]
        public int FiscalYear { get; set; }

        [Required]
        public string FiscalYearName { get; set; }

        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public string OpenedBy { get; set; }

        [Required]
        public DateTime OpenedOn { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string ClosedBy { get; set; }

        [Required]
        public DateTime? ClosedOn { get; set; }
    }

    public class ReportingPeriod : ReplicatedOnByFields
    {
        [Key]
        [Required]
        public int PeriodID { get; set; }

        [Required]
        public string PeriodName { get; set; }

        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public int FiscalYearId { get; set; }

        [Required]
        public FiscalYears FiscalYear { get; set; }

        [Required]
        public string PeriodCode { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }
    }

    public class SummaryForMIS
    { 
        [Key]
        [Required]
        public int ID { get; set; }

        [Required]
        public string FormName { get; set; }

        [Required]
        public string Reason { get; set; }

        [Required]
        public string Action { get; set; }
    }

    public class CreatedOnByFields
    {
        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }
    }

    public class CreatedByApproveByFields
    {
        [Required]
        public string CreatedById { get; set; }

        public Users CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [Required]
        public string ApproveById { get; set; }

        public Users ApproveBy { get; set; }

        [Required]
        public DateTime ApprovedOn { get; set; }
    }

    public class ReplicatedOnByFields
    {
        public bool IsReplicated { get; set; }

        public Int64? ReplicatedById { get; set; }

        public Users ReplicatedBy { get; set; }

        public DateTime? ReplicatedOn { get; set; }
    }

    public class ApiStatus
    {
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int? Id { get; set; }
    }

    public class UserDevice
    {
        [Key]
        public Int64 Id { get; set; }
        public Guid DataId { get; set; }
        public string ModuleName { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public bool IsDevice { get; set; }
    }

    public class RetargetingHeader:ReplicatedOnByFields
    {
        [Key]
        public string RetargetingHeaderID { get; set; }
        public string FormID { get; set; }
        public string KebeleId { get; set; }
        public Kebele Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseholdHead { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string FiscalYear { get; set; }
        public DateTime DateUpdated { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public int CreatedById { get; set; }
        public Users CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool ApprovalStatus { get; set; }
        public int ApprovedById { get; set; }
        public Users ApprovedBy { get; set; }
        public DateTime ApprovedOn { get; set; }
        public string ReportFilePath { get; set; }
    }

    public class RetargetingDetail : ReplicatedOnByFields
    {
        [Key]
        public Guid RetargetingDetailId { get; set; }
        public Guid RetargetingHeaderId { get; set; }
        public Guid RetargetingHeader { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Int32 Age { get; set; }
        public string Sex { get; set; }
        public string ClientCategory { get; set; }
        public string Status { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string IsUpdated { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime StartDateTDS { get; set; }
        public DateTime NextCNStatusDate { get; set; }
        public DateTime EndDateTDS { get; set; }
        public DateTime DateTypeCertificate { get; set; }
        public string CaretakerID { get; set; }
        public string ChildID { get; set; }
    }

    public class ChildProtectionHeader : ReplicatedOnByFields
    {
        [Key]
        public Guid ChildProtectionHeaderID { get; set; }
        public Guid ChildProtectionId { get; set; }
        public string CaseManagementReferral { get; set; }
        public string CaseStatusId { get; set; }
        public string ClientTypeID { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedOn { get; set; }
        public string ClosedBy { get; set; }
        public DateTime ClosedOn { get; set; }
        public string ClosedReason { get; set; }
        public string ReportFilePath { get; set; }
        public string FiscalYear { get; set; }
        public string ChildDetailID { get; set; }

    }

    public class ChildProtectionDetail : ReplicatedOnByFields
    {
        [Key]
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public ChildProtectionHeader ChildProtectionHeader { get; set; }
        public int RiskId { get; set; }
        public int ServiceId { get; set; }
        public int ProviderId { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

    }
}