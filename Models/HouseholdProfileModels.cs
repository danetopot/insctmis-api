﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace INSCTMIS.Api.Models
{
    public class ProfileDSHeader : CreatedByApproveByFields
    {

        [Key]
        public Guid ProfileDSHeaderID { get; set; }

        public int ProfileDSHeaderID_Old { get; set; }

        [Required]
        public int KebeleId { get; set; }

        public Kebele Kebele { get; set; }

        public string Gote { get; set; }

        public string Gare { get; set; }

        [Required]
        public string NameOfHouseHoldHead { get; set; }

        [Required]
        public string HouseHoldIDNumber { get; set; }

        [Required]
        public DateTime CollectionDate { get; set; }

        [Required]
        public string SocialWorker { get; set; }


        [Required]
        public string CCCCBSPCMember { get; set; }

        [Required]
        public string CBHIMembership { get; set; }

        [Required]
        public string CBHINumber { get; set; }

        public string ReportFilePath { get; set; }

        public bool IsReplicated { get; set; }

        public Int64? ReplicatedById { get; set; }

        public Users ReplicatedBy { get; set; }

        public DateTime? ReplicatedOn { get; set; }

        public string Remarks { get; set; }

        public string FiscalYear { get; set; }

        public string RetargetingReferenceID { get; set; }
    }

    public class ProfileDSDetail : ReplicatedOnByFields
    {
        [Key]
        public Guid ProfileDSDetailID { get; set; }

        public Guid ProfileDSHeaderId { get; set; }

        public ProfileDSHeader ProfileDSHeader { get; set; }

        public int ProfileDSDetailID_Old { get; set; }

        public int ProfileDSHeaderID_Old { get; set; }

        [Required]
        public string HouseHoldMemberName { get; set; }

        [Required]
        public string IndividualID { get; set; }

        [Required]
        public string MedicalRecordNumber { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public string Sex { get; set; }

        [Required]
        public string PWL { get; set; }

        [Required]
        public string Handicapped { get; set; }

        [Required]
        public string ChronicallyIll { get; set; }

        [Required]
        public string NutritionalStatus { get; set; }

        [Required]
        public string EnrolledInSchool { get; set; }

        public string SchoolName { get; set; }

        public string Grade { get; set; }

        [Required]
        public string ChildProtectionRisk { get; set; }


        public bool Form2Produced { get; set; }

        public bool Form3Generated { get; set; }

        public bool Form4Produced { get; set; }

        public bool Form4Captured { get; set; }

        [Required]
        public string Pregnant { get; set; }

        [Required]
        public string Lactating { get; set; }

        [Required]
        public string childUnderTSForCMAM { get; set; }
    }


    public class ProfileTDSPLW : CreatedByApproveByFields
    {
        public Guid ProfileTDSPLWID { get; set; }
        public int ProfileTDSPLWID_Old { get; set; }
        [Required]
        public int KebeleId { get; set; }
        public Kebele Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        [Required]
        public string NameOfPLW { get; set; }
        [Required]
        public string HouseHoldIDNumber { get; set; }
        [Required]
        public string MedicalRecordNumber { get; set; }
        [Required]
        public int PLWAge { get; set; }
        [Required]
        public string PLW { get; set; }
        public DateTime? StartDateTDS { get; set; }
        public DateTime? EndDateTDS { get; set; }
        [Required]
        public string NutritionalStatusPLW { get; set; }
        [Required]
        public DateTime CollectionDate { get; set; }
        [Required]
        public string SocialWorker { get; set; }
        [Required]
        public string CCCCBSPCMember { get; set; }
        [Required]
        public string ChildProtectionRisk { get; set; }
        [Required]
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public bool Form2Produced { get; set; }
        public bool Form3Generated { get; set; }
        public bool Form4Produced { get; set; }
        public bool Form4Captured { get; set; }
        public string FilePath { get; set; }
        public Int64? ReplicatedById { get; set; }

        public Users ReplicatedBy { get; set; }

        public DateTime? ReplicatedOn { get; set; }
        public string Remarks { get; set; }
        [Required]
        public string FiscalYear { get; set; }
        public string RetargetingReferenceID { get; set; }
    }

    public class ProfileTDSPLWDetail : ReplicatedOnByFields
    {
        [Key]
        public Guid ProfileTDSPLWDetailID { get; set; }

        [Required]
        public Guid ProfileTDSPLWId { get; set; }
        public ProfileTDSPLW ProfileTDSPLW { get; set; }
        [Required]
        public DateTime BabyDateOfBirth { get; set; }
        [Required]
        public string BabyName { get; set; }
        [Required]
        public string BabySex { get; set; }
        [Required]
        public string NutritionalStatusInfant { get; set; }

    }

    public class ProfileTDSCMC : CreatedByApproveByFields
    {
        [Required]
        public Guid ProfileTDSCMCID { get; set; }
        [Required]
        public string ProfileTDSCMCID_Old { get; set; }
        [Required]
        public int KebeleId { get; set; }
        public Kebele Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        [Required]
        public string NameOfCareTaker { get; set; }
        [Required]
        public string HouseHoldIDNumber { get; set; }
        [Required]
        public string MalnourishedChildName { get; set; }
        [Required]
        public string MalnourishedChildSex { get; set; }
        [Required]
        public string ChildDateOfBirth { get; set; }
        [Required]
        public DateTime DateTypeCertificate { get; set; }
        [Required]
        public string MalnourishmentDegree { get; set; }
        [Required]
        public DateTime StartDateTDS { get; set; }
        [Required]
        public DateTime NextCNStatusDate { get; set; }
        [Required]
        public DateTime EndDateTDS { get; set; }
        [Required]
        public DateTime CollectionDate { get; set; }
        [Required]
        public string SocialWorker { get; set; }
        [Required]
        public string CCCCBSPCMember { get; set; }
        [Required]
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        [Required]
        public string ChildProtectionRisk { get; set; }
        [Required]
        public string CaretakerID { get; set; }
        [Required]
        public string ChildID { get; set; }
        public bool Form2Produced { get; set; }
        public bool Form3Generated { get; set; }
        public bool Form4Produced { get; set; }
        public bool Form4Captured { get; set; }
        public string FilePath { get; set; }
        public string IsReplicated { get; set; }
        public string ReplicatedBy { get; set; }
        public string ReplicatedOn { get; set; }
        public string Remarks { get; set; }
        [Required]
        public string FiscalYear { get; set; }
        public string RetargetingReferenceID { get; set; }

    }
}