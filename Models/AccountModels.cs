﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace INSCTMIS.Api.Models
{

    public class Users
    {
        [Key]
        [Required]
        public Int64 UserId { get; set; }

        public Int32 RoleId { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
    
}