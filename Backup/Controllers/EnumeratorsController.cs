﻿using MCHMIS.Api.Data;
using MCHMIS.Api.Helpers;
using MCHMIS.Api.Models;
using MCHMIS.Api.ViewModels;
using System.Linq;
using System.Web.Http;

namespace MCHMIS.Api.Controllers
{

    /// <summary>
    /// Enumerators End Point
    /// </summary>
    [AllowAnonymous]
    public class EnumeratorsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Login Enumerator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Enumerators/Login/")]
        [HttpPost]
        public IHttpActionResult Login(LoginVM model)
        {

            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetLoginData(enumerator.Id);

            return Ok(returnModel);



        }



        public CvSetupVm GetLoginData(int id)
        {
            var returnModel = new CvSetupVm
            {
                Enumerator = db.Enumerators.FirstOrDefault(x => x.Id == id),


                Wards = (db.Wards.Select(x => new WardVm()
                {
                    Id = x.Id,
                    Name = x.Name

                }).Distinct().ToList()),

                SubLocations = (db.SubLocations.Select(x => new SubLocationVm()
                {
                    Id = x.Id,
                    Name = x.Name

                }).Distinct().ToList()),



                SystemCodes = (from c in this.db.SystemCodes
                               where (c.SystemModuleId == null)
                               select new SystemCodeVm()
                               {
                                   Id = c.Id,
                                   Code = c.Code,
                                   Description = c.Description
                               }).Distinct().ToList(),

                SystemCodeDetails =
                (from c in this.db.SystemCodeDetails
                 where (c.SystemCode.SystemModuleId == null)
                 select new SystemCodeDetailVm()
                 {
                     Id = c.Id,
                     Code = c.Code,
                     OrderNo = c.OrderNo,
                     SystemCodeId = c.SystemCodeId
                 }).Distinct().ToList()
            };


            return returnModel;
        }
        /// <summary>
        /// Logout Enumerator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Enumerators/Logout/")]
        [HttpPost]
        public IHttpActionResult Logout(LoginVM model)
        {

            return Ok(model);
        }


        /// <summary>
        /// Enumerator Change Pin EndPoint
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Enumerators/ChangePin/")]
        [HttpPost]
        public IHttpActionResult ChangePin(LoginVM model)
        {

            return Ok(model);
        }


        /// <summary>
        /// Enumerator Reset Pin EndPoint
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Enumerators/ResetPin/")]
        [HttpPost]
        public IHttpActionResult ResetPin(LoginVM model)
        {

            return Ok(model);
        }



        //[Route("api/Enumerators/PullSettings/")]
        //[HttpPost]
        //public IHttpActionResult PullSettings(LoginVM model)
        //{
        //    var returnModel = new CvSetupVm
        //    {
        //        SystemCodes = db.SystemCodes.ToList(),
        //        SystemCodeDetails = db.SystemCodeDetails.ToList()
        //    };
        //    return Ok(returnModel);


        //}

    }
}
