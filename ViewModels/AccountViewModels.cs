﻿using System;

namespace INSCTMIS.Custom.Api.ViewModels
{

    public class UserVm
    {
        public Int64 UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int64 RoleID { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? LastActivityDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public DateTime? LastPasswordChangedDate { get; set; }
        public bool IsOnLine { get; set; }
        public bool IsLockedOut { get; set; }
        public DateTime? LastLockedOutDate { get; set; }
        public bool IsReplicated { get; set; }
        public int ReplicatedBy { get; set; }
        public DateTime? ReplicatedOn { get; set; }
    }

    public class LoginVM
    {
        public Int64 UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Message { get; set; }
        public string IsAuthenticated { get; set; }
    }

    public class LoginRequest
    {
        public string AuthKey { get; set; }

    }
    public class LoginResponse
    {
        public int Success { get; set; }
    }

}