﻿using INSCTMIS.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace INSCTMIS.Api.ViewModels
{
    public class SetupVm : ErrorVm
    {
        public Users SocialWorker { get; set; }
        public List<RegionVm> Region { get; set; }
        public List<WoredaVm> Woreda { get; set; }
        public List<KebeleVm> Kebele { get; set; }
        public List<ServiceProviderVm> ServiceProvider { get; set; }
        public List<IntegratedServiceVm> IntegratedService { get; set; }
        public List<MonitoringServiceVm> MonitoringService { get; set; }
        public List<SystemCodeStringVm> SystemCodeDetail { get; set; }
    }

    public class RegionVm
    {
        public int RegionId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public class WoredaVm
    {
        public int WoredaId { get; set; }
        public int RegionId { get; set; }
        public Region Region { get; set; }
        public string WoredaCode { get; set; }
        public string WoredaName { get; set; }
    }

    public class KebeleVm
    {
        public int KebeleId { get; set; }
        public int WoredaId { get; set; }
        public Woreda Woreda  { get; set; }
        public string KebeleCode { get; set; }
        public string KebeleName { get; set; }
    }

    public class ServiceProviderVm
    {
        public int ServiceProviderId { get; set; }
        public string ServiceProviderName { get; set; }
    }

    public class IntegratedServiceVm
    {
        public int ServiceId { get; set; }
        public int ServiceProviderId { get; set; }
        public ServiceProvider ServiceProvider { get; set; }
        public string ServiceName { get; set; }
    }

    public class MonitoringServiceVm
    {
        public int Id { get; set; }
        public string FormName { get; set; }
        public string Reason { get; set; }
        public string Action { get; set; }
    }

    public class SystemCodeVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SystemCode { get; set; }
    }

    public class SystemCodeStringVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string SystemCode { get; set; }
    }

    public class ErrorVm : SuccessVm
    {
        public int No { get; set; }
        public string Error { get; set; }
    }

    public class SuccessVm
    {
        public string Success { get; set; }
    }
}