﻿using INSCTMIS.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace INSCTMIS.Custom.Api.ViewModels
{
    public class MonitoringVm
    {
        public List<Monitoring5A1Vm> TDS1Households { get; set; }
        public List<Monitoring5A1MembersVm> TDS1HouseholdMembers { get; set; }
        public List<Monitoring5A2Vm> TDS2Households { get; set; }
        public List<Monitoring5A2MembersVm> TDS2HouseholdMembers { get; set; }
        public List<Monitoring5BVm> PLWHouseholds { get; set; }
        public List<Monitoring5BMembersVm> PLWHouseholdMembers { get; set; }
        public List<Monitoring5CVm> CMCHouseholds { get; set; }
        public List<Monitoring5CMembersVm> CMCHouseholdMembers { get; set; }
        public string Message { get; set; }
    }

    public class Monitoring5A1Vm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public int Members { get; set; }
    }
    public class Monitoring5A1MembersVm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string ReportingPeriod { get; set; }
        public Int32 KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string HouseHoldMemberName { get; set; }
    }

    public class Monitoring5A2Vm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string ReportingPeriod { get; set; }
        public Int32 KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public int Members { get; set; }
    }
    public class Monitoring5A2MembersVm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string ReportingPeriod { get; set; }
        public Int32 KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string HouseHoldMemberName { get; set; }
    }


    public class Monitoring5BVm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfPLW { get; set; }
    }
    public class Monitoring5BMembersVm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ReportingPeriod { get; set; }
        public Int32 KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public DateTime? BabyDateOfBirth { get; set; }
    }

    public class Monitoring5CVm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfCareTaker { get; set; }
    }
    public class Monitoring5CMembersVm
    {
        public Int64 ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ReportingPeriod { get; set; }
        public Int32 KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
    }

    #region MyRegion
    /*
    public class MonitoringCPHouseholdVm
    {
        public Int64 ColumnID { get; set; }
        public string ChildProtectionHeaderId { get; set; }
        public string ProfileDSHeaderId { get; set; }
        public string ProfileDSDetailId { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string WoredaName { get; set; }
        public string GoteGare { get; set; }
        public string ClientType { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string FiscalYear { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? ClosedOn { get; set; }
        public string ClosedBy { get; set; }
        public string ClosedReason { get; set; }
        public string CaseStatus { get; set; }
    }

    public class MonitoringCPCasesVm
    {
        public Int64 ColumnID { get; set; }
        public string ChildProtectionDetailId { get; set; }
        public string ChildProtectionHeaderId { get; set; }
        public ChildProtectionHeader ChildProtectionHeader { get; set; }
        public int RiskId { get; set; }
        public int ServiceId { get; set; }
        public int ProviderId { get; set; }
    }

    
    */
    #endregion

    public class MonitoringParameterVm
    {
        public Int64 KebeleID { get; set; }
        public Int64 ReportingPeriodID { get; set; }
    }

    public class MonitoringCaptureVm
    {
        public Guid IndividualProfileID { get; set; }
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public DateTime CompletedDate { get; set; }
        public int NonComplianceReason { get; set; }
        public int ActionResponse { get; set; }
        public string HouseholdType { get; set; }
        public string SocialWorker { get; set; }
        public Int64 CreatedById { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class TabletMonitoringViewModel
    {
        [Required]
        public string MonitoringInfo { get; set; }
    }
}