﻿using INSCTMIS.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace INSCTMIS.Custom.Api.ViewModels
{
    public class ComplianceTDSVm
    {
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
    }

    public class ComplianceTDSMembersVm
    {
        public Int64 ColumnID { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string NutritionalStatus { get; set; }
        public string EnrolledInSchool { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class ComplianceTDSPLWVm
    {
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
    }

    public class ComplianceTDSPLWMembersVm
    {
        public Int64 ColumnID { get; set; }
        public Guid ProfileTDSPLWID { get; set; }
        public Guid? ProfileTDSPLWDetailID { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public Int32 PLWAge { get; set; }
        public string BabySex { get; set; }
        public string PLW { get; set; }
        public string BabyName { get; set; }
        public DateTime? BabyDateOfBirth { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class ComplianceTDSCMCVm
    {
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
    }

    public class ComplianceTDSCMCMembersVm
    {
        public Int64 ColumnID { get; set; }
        public Guid ProfileTDSCMCID { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public Guid ProfileTDSCMCIDDetail { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MalnourishmentDegree { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class ComplianceCPVm
    {
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string FiscalYear { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public string ClientType { get; set; }
    }

    public class ComplianceCPMembersVm
    {
        public Int64 ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldMemberSex { get; set; }
        public Int32 HouseHoldMemberAge { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string ClientType { get; set; }
    }

    public class ComplianceCPFollowUpVm
    {
        public Int64 UniqueID { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildDetailID { get; set; }
        public string ClientTypeID { get; set; }
        public int RiskId { get; set; }
        public string RiskName { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceVm
    {
        public List<ComplianceTDSVm> TDSHouseholds { get; set; }
        public List<ComplianceTDSMembersVm> TDSHouseholdMembers { get; set; }
        public List<ComplianceTDSPLWVm> TDSPLWHouseholds { get; set; }
        public List<ComplianceTDSPLWMembersVm> TDSPLWHouseholdMembers { get; set; }
        public List<ComplianceTDSCMCVm> TDSCMCHouseholds { get; set; }
        public List<ComplianceTDSCMCMembersVm> TDSCMCHouseholdMembers { get; set; }
        public List<ComplianceCPVm> CPHouseholds { get; set; }
        public List<ComplianceCPMembersVm> CPHouseholdMembers { get; set; }
        public List<ComplianceCPFollowUpVm> CPCases { get; set; }
        public string Error { get; set; }
    }

    public class ComplianceParameterVm
    {
        public Int64 KebeleID { get; set; }
        public Int64 ReportingPeriodID { get; set; }
    }

    public class ComplianceCaptureVm
    {
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public Guid IndividualProfileID { get; set; }
        public int ServiceID { get; set; }
        public string HouseholdType { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public int CreatedById { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class TabletComplianceViewModel
    {
        [Required]
        public string ComplianceInfo { get; set; }
    }

    public class ISNPCaseVm
    {
        public ComplianceCPCaseHeaderCaptureVm CPCaseHeaderInfo { get; set; }
        public List<ComplianceCPCaseDetailCaptureVm> CPCaseDetailInfo { get; set; }
        public ComplianceCPCaseVisitHeaderCaptureVm CPCaseVisitHeaderInfo { get; set; }
        public List<ComplianceCPCaseVisitDetailCaptureVm> CPCaseVisitDetailInfo { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class ComplianceCPCaseHeaderCaptureVm
    {
        public Guid ChildProtectionHeaderID { get; set; }
        public string ChildProtectionId { get; set; }
        public string CaseManagementReferral { get; set; }
        public string CaseStatusId { get; set; }
        public string ClientTypeID { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string Remarks { get; set; }
        public string FiscalYear { get; set; }
        public Guid ChildDetailID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class ComplianceCPCaseDetailCaptureVm
    {
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public string ChildName { get; set; }
        public string RiskId { get; set; }
        public string RiskName { get; set; }
        public string ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ProviderId { get; set; }
        public string ProviderName { get; set; }
    }

    public class ComplianceCPCaseVisitHeaderCaptureVm
    {
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public DateTime VisitDate { get; set; }
        public string FiscalYear { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class ComplianceCPCaseVisitDetailCaptureVm
    {
        public Guid ChildProtectionVisitDetailId { get; set; }
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildDetailID { get; set; }
        public string IsServiceAccessed { get; set; }
        public DateTime? DateServiceAccessed { get; set; }
        public string ServiceNotAccessedReason { get; set; }
        public string ChildName { get; set; }
        public string RiskName { get; set; }
        public string ServiceName { get; set; }
        public string ProviderName { get; set; }
    }

    public class TabletISNPViewModel
    {
        [Required]
        public string ISNPInfo { get; set; }
    }

    public class RecordCountVm
    {
        public int TotalRecords { get; set; }
    }
}