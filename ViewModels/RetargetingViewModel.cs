﻿using INSCTMIS.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace INSCTMIS.Custom.Api.ViewModels
{
    public class RetargetVm
    {
        public List<RetargetingVm> RetargetingHouseholds { get; set; }
        public List<RetargetingMembersVm> RetargetingHouseholdMembers { get; set; }
        public string Message { get; set; }
    }

    public class RetargetingVm
    {
        public string RetargetingHeaderID { get; set; }
        public string FormID { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseholdHead { get; set; }
        public string HouseholdIDNumber { get; set; }
        public int ClientCategory { get; set; }
        public int Members { get; set; }
        public int FiscalYear { get; set; }
        public bool ApprovalStatus { get; set; }
    }

    public class RetargetingMembersVm 
    {
        public string RetargetingDetailID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
        public int ClientCategory { get; set; }
        public string StartDateTDS { get; set; }
        public DateTime? NextCNStatusDate { get; set; }
        public string EndDateTDS { get; set; }
        public string DateTypeCertificate { get; set; }
        public string CaretakerID { get; set; }
        public string ChildID { get; set; }
        public string CCCMember { get; set; }
        public string Status { get; set; }
    }

    public class RetargetingParameterVm
    {
        public int KebeleID { get; set; }
        public int FiscalYear { get; set; }
    }

    public class RetargetingCaptureVm
    {
        public string RetargetingDetailID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string CCCMember { get; set; }
        public string Status { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string SocialWorker { get; set; }
        public DateTime DateUpdated { get; set; }
        public bool IsUpdated { get; set; }
        public Int64 CreatedBy { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class TabletRetargetingViewModel
    {
        [Required]
        public string RetargetingInfo { get; set; }
    }
}