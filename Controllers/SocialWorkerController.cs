﻿using INSCTMIS.Api.Models;
using INSCTMIS.Api.ViewModels;
using INSCTMIS.Custom.Api.Data;
using INSCTMIS.Custom.Api.Helpers;
using INSCTMIS.Custom.Api.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace INSCTMIS.Api.Controllers
{
    /// <summary>
    /// Social Worker End Point
    /// </summary>
    [AllowAnonymous]
    public class SocialWorkerController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Login SocialWorker
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/SocialWorker/Login/")]
        [HttpPost]
        public IHttpActionResult Login(LoginVM model)
        {
            string hashPin;
            Users user = null;
            var data = JsonConvert.SerializeObject(model);
            FileLog($":::::::::::::::::::::::::::: DataInput@api/SocialWorker/Login ::::::::::::::::::::::::::::\n\n" + data);

            try
            {
                if (string.IsNullOrEmpty(model.UserName) && string.IsNullOrEmpty(model.Password))
                {
                    model.Message = "Your Username and Password is required";
                    model.IsAuthenticated = "1";
                    return Ok(model);
                }

                if (!string.IsNullOrEmpty(model.UserName) && !string.IsNullOrEmpty(model.Password))
                {
                    var username = model.UserName;
                    var password = model.Password;
                    hashPin = EasyMD5.Hash(model.Password);
                    user = db.User.FirstOrDefault(x => x.Username == username && x.Password == hashPin);
                    if(user != null)
                    {
                        model.Message = "Login Success";
                        model.IsAuthenticated = "1";
                        FileLog(":::::::::::::::::::::::::::: Success@api/SocialWorker/Login ::::::::::::::::::::::::::::");
                        return Ok(model);
                    }
                }

                if (user == null)
                {
                    model.Message = "Your Account Does not exist or is Deactivated. \n Check Username and Password and Try Again";
                    model.IsAuthenticated = "0";
                    return Ok(model);
                }

                FileLog($":::::::::::::::::::::::::::: DataOutput@api/SocialWorker/Login ::::::::::::::::::::::::::::\n\n" + JsonConvert.SerializeObject(model));
                return Ok(model);
            }
            catch (Exception ex)
            {
                model.Message = $"Error : - " + ex.ToString();
                model.IsAuthenticated = "1";
                FileLog(":::::::::::::::::::::::::::: Error@api/SocialWorker/Login : " + ex.Message + "\n\n" + ex.InnerException + " ::::::::::::::::::::::::::::");
                return Ok(model);
                
            }
        }

        /// <summary>
        /// SocialWorker Change Pin EndPoint
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/SocialWorker/ChangePin/")]
        [HttpPost]
        public IHttpActionResult ChangePin(LoginVM model)
        {

            return Ok(model);
        }


        /// <summary>
        /// SocialWorker Reset Pin EndPoint
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/SocialWorker/ResetPin/")]
        [HttpPost]
        public IHttpActionResult ResetPin(LoginVM model)
        {

            return Ok(model);
        }

        /// <summary>
        /// SocialWorker GetListingSettings
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/SocialWorker/GetListingSettings/")]
        [HttpPost]
        public IHttpActionResult GetListingSettings(LoginVM model)
        {
            var returnModel = new SetupVm();

            try
            {
                returnModel = GetLoginData(model.UserName, EasyMD5.Hash(model.Password));
                FileLog(":::::::::::::::::::::::::::: Success@api/SocialWorker/GetListingSettings ::::::::::::::::::::::::::::");
                return Ok(returnModel);
            }
            catch (Exception ex)
            {
                returnModel.Error = $"Error : - " + ex.ToString();
                FileLog(":::::::::::::::::::::::::::: Error@api/SocialWorker/GetListingSettings : " + ex.Message + "\n\n" + ex.InnerException + " ::::::::::::::::::::::::::::");
                return Ok(returnModel);
            };
        }

        public SetupVm GetLoginData(string username, string password)
        {
            var returnModel = new SetupVm();

            try
            {
                returnModel = new SetupVm
                {
                    SocialWorker = db.User.FirstOrDefault(x => x.Username == username && x.Password == password),

                    
                    Region = GetRegions().ToList(),
                   
                    Woreda = GetWoredas().ToList(),

                    Kebele = GetKebeles().ToList(),
                    
                    ServiceProvider = GetServiceProviders().ToList(),

                    IntegratedService = GetIntegratedServices().ToList(),

                    MonitoringService = GetMonitoringServices().ToList(),

                    SystemCodeDetail = GetSystemCodeDetails().ToList(),

                    Success = "Login Successful"
                };

                FileLog(":::::::::::::::::::::::::::: Success@GetLoginData ::::::::::::::::::::::::::::");
            }
            catch(Exception ex)
            {
                FileLog(":::::::::::::::::::::::::::: Error@GetLoginData : " + ex.Message + "\n\n" + ex.InnerException + " ::::::::::::::::::::::::::::");
            }

            FileLog($":::::::::::::::::::::::::::: DataOutput@GetLoginData ::::::::::::::::::::::::::::\n\n" + JsonConvert.SerializeObject(returnModel));
            return returnModel;
        }



        #region Helpers

        public IEnumerable<RegionVm> GetRegions()
        {
            return (from c in this.db.Region
                    select new RegionVm()
                    {
                        RegionId = c.RegionID,
                        RegionCode = c.RegionCode,
                        RegionName = c.RegionName
                    }).Distinct().ToList();
        }

        public IEnumerable<WoredaVm> GetWoredas()
        {
            return (from c in this.db.Woreda
                    select new WoredaVm()
                    {
                        WoredaId = c.WoredaID,
                        WoredaCode = c.WoredaCode,
                        WoredaName = c.WoredaName,
                        RegionId = c.RegionID
                    }).Distinct().ToList();
        }

        public IEnumerable<KebeleVm> GetKebeles()
        {
            return (from c in this.db.Kebele
                    select new KebeleVm()
                    {
                        KebeleId = c.KebeleID,
                        KebeleCode = c.KebeleCode,
                        KebeleName = c.KebeleName,
                        WoredaId = c.WoredaID
                    }).Distinct().ToList();
        }

        public IEnumerable<ServiceProviderVm> GetServiceProviders()
        {
            return (from c in this.db.ServiceProvider
                    select new ServiceProviderVm()
                    {
                        ServiceProviderId = c.ServiceProviderID,
                        ServiceProviderName = c.ServiceProviderName
                    }).Distinct().ToList();
        } 

        public IEnumerable<IntegratedServiceVm> GetIntegratedServices()
        {
            return (from c in this.db.IntegratedService
                    select new IntegratedServiceVm()
                    {
                        ServiceId = c.ServiceID,
                        ServiceName = c.ServiceName,
                        ServiceProviderId = c.ServiceProviderID
                    }).Distinct().ToList();
        }

        public IEnumerable<SystemCodeStringVm> GetSystemCodeDetails()
        {
            var options = new List<SystemCodeStringVm>();

            options.AddRange(this.GetFiscalYears());
            options.AddRange(this.GetReportingPeriods());
            options.AddRange(this.GetYesNoOptions());
            options.AddRange(this.GetGenderOptions());
            options.AddRange(this.GetClientTypeOptions());
            options.AddRange(this.GetRiskTypeOptions());
            options.AddRange(this.GetChildProtectionServiceOptions());
            options.AddRange(this.GetChildProtectionServiceProviderOptions());
            options.AddRange(this.GetReasonsServiceNotAccessibleOptions());
            options.AddRange(this.GetNutritionalStatusOptions());
            options.AddRange(this.GetClientStatusOptions());
            options.AddRange(this.GetSchoolGradeOptions());
            options.AddRange(this.GetPLWOptions());

            for (int i = 0; i < options.Count(); i++)
            {
                options[i].Id = (i + 1);
            }

            return options;
        }

        public IEnumerable<SystemCodeStringVm> GetFiscalYears()
        {
            return (from c in this.db.FiscalYear
                    select new SystemCodeStringVm()
                    {
                        Id = 0,
                        Name = c.FiscalYearName,
                        Value = c.FiscalYear.ToString(),
                        SystemCode = "FiscalYear"
                    }).Distinct().ToList();
        }

        public IEnumerable<SystemCodeStringVm> GetReportingPeriods()
        {
            return (from c in this.db.ReportingPeriod
                    select new SystemCodeStringVm()
                    {
                        Id = 0,
                        Name = c.PeriodName,
                        Value = c.PeriodID.ToString(),
                        SystemCode = "ReportingPeriod"
                    }).Distinct().ToList();
        }

        public IEnumerable<MonitoringServiceVm> GetMonitoringServices()
        {
            return (from c in this.db.MonitoringService
                    select new MonitoringServiceVm()
                    {
                        Id = c.ID,
                        FormName = c.FormName,
                        Reason = c.Reason,
                        Action = c.Action
                    }).Distinct().ToList();
        }

        public IEnumerable<SystemCodeStringVm> GetYesNoOptions()
        {
            List<SystemCodeStringVm> YesNoOptions = new List<SystemCodeStringVm> {
                                             new SystemCodeStringVm{ Id=0, Value = "0", Name = "Select", SystemCode = "YesNoOptions"},
                                             new SystemCodeStringVm{ Id=0, Value = "NO",  Name = "NO", SystemCode = "YesNoOptions"},
                                             new SystemCodeStringVm{ Id=0, Value = "YES", Name = "YES", SystemCode = "YesNoOptions"}
                                             };

            return YesNoOptions;
        }

        public IEnumerable<SystemCodeStringVm> GetGenderOptions()
        {
            List<SystemCodeStringVm> GenderOptions = new List<SystemCodeStringVm> {
                                             new SystemCodeStringVm{ Id=0, Value = "0",   Name = "Select", SystemCode = "GenderOptions"},
                                             new SystemCodeStringVm{ Id=0, Value = "M",   Name = "Male", SystemCode = "GenderOptions"},
                                             new SystemCodeStringVm{ Id=0, Value = "F", Name = "Female", SystemCode = "GenderOptions"}
                                             };

            return GenderOptions;
        }

        public IEnumerable<SystemCodeStringVm> GetClientTypeOptions()
        {
            List<SystemCodeStringVm> ClientTypeOptions = new List<SystemCodeStringVm> {
                                            new SystemCodeStringVm{ Id=0, Value= "0", Name = "Select", SystemCode = "ClientTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value= "PDS", Name = "Permanent Direct Support(PDS)", SystemCode = "ClientTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value= "PLW", Name = "Temporary Direct Support(PLW)", SystemCode = "ClientTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "CMC", Name = "Temporary Direct Support(CMC)", SystemCode = "ClientTypeOptions" }
                                             };

            return ClientTypeOptions;
        }

        public IEnumerable<SystemCodeStringVm> GetRiskTypeOptions()
        {
            List<SystemCodeStringVm> RiskTypeOptions = new List<SystemCodeStringVm> {
                                            new SystemCodeStringVm{ Id=0, Value = "0", Name = "Select", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "1", Name = "Maltreatment", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "2", Name = "Bullying", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "3", Name = "Youth violence", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "4", Name = "Intimate partner violence", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "5", Name = "Sexual violence", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "6", Name = "Emotional or psychological violence", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "7", Name = "Gender-based violence", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "8", Name = "Children without adequate parental care", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "9", Name = "Child trafficking", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "10", Name = "Children living with disabilities at risk of exclusion", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "11", Name = "Child labour", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "12", Name = "Children in contact with the law", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "13", Name = "Children on the move", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "14", Name = "Children at risk of harmful practices", SystemCode = "RiskTypeOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "15", Name = "Children in street situations", SystemCode = "RiskTypeOptions" }
                                             };

            return RiskTypeOptions;
        }

        public IEnumerable<SystemCodeStringVm> GetChildProtectionServiceOptions()
        {
            List<SystemCodeStringVm> ChildProtectionServiceOptions = new List<SystemCodeStringVm> {
                                            new SystemCodeStringVm{ Id=0, Value = "9", Name = "Select", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "0", Name = "No Services Provided", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "1", Name = "Food and Nutrition", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "2", Name = "Shelter", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "3", Name = "Economic Strengthening", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "4", Name = "Care", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "5", Name = "Justice Services(including by police, Office of Justice and courts)", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "6", Name = "Health", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "7", Name = "Psycho-Social", SystemCode = "ChildProtectionServiceOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "8", Name = "Education and Skills Training", SystemCode = "ChildProtectionServiceOptions" }
                                             };

            return ChildProtectionServiceOptions;
        }

        public IEnumerable<SystemCodeStringVm> GetChildProtectionServiceProviderOptions()
        {
            List<SystemCodeStringVm> ChildProtectionServiceProviderOptions = new List<SystemCodeStringVm> {
                                            new SystemCodeStringVm{ Id=0, Value = "0", Name = "Select", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "1", Name = "Woreda Office of Women and Children Affairs", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "2", Name = "Woreda Office of Labour and Social Affairs", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "3", Name = "Woreda Office of Agriculture/Food Security", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "4", Name = "Woreda Micro Enterprise Development Office", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "5", Name = "Health Center", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "6", Name = "Private Health Facility", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "7", Name = "School", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "8", Name = "NGO", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "9", Name = "Police", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "10", Name = "Justice Office", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "11", Name = "Court", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "12", Name = "CCC", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "13", Name = "Community-Based Organization", SystemCode = "ChildProtectionServiceProviderOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "14", Name = "Other", SystemCode = "ChildProtectionServiceProviderOptions" }
                                             };

            return ChildProtectionServiceProviderOptions;
        }

        public IEnumerable<SystemCodeStringVm> GetReasonsServiceNotAccessibleOptions()
        {
            List<SystemCodeStringVm> ReasonsServiceNotAccessibleOptions = new List<SystemCodeStringVm> {
                                            new SystemCodeStringVm{ Id=0, Value = "0", Name = "Select", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "1", Name = "Services not available at the time the client sought the services", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "2", Name = "Client not willing to access services", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "3", Name = "Service site too far for client", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "4", Name = "Service provider not willing to provide services", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "5", Name = "Client has no trust in services or service provider for personal reasons", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "6", Name = "Client prevented to access services by a third party including apprehension", SystemCode = "ReasonsServiceNotAccessibleOptions" },
                                            new SystemCodeStringVm{ Id=0, Value = "7", Name = "Other", SystemCode = "ReasonsServiceNotAccessibleOptions" }
                                            };

            return ReasonsServiceNotAccessibleOptions;

        }

        public IEnumerable<SystemCodeStringVm> GetNutritionalStatusOptions()
        {
            List<SystemCodeStringVm> GetNutritionalStatusOptions = new List<SystemCodeStringVm> {
                                             new SystemCodeStringVm{ Id=0, Value = "0", Name="Select", SystemCode = "NutritionalStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "N", Name="Normal", SystemCode = "NutritionalStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "M", Name="Mod.", SystemCode = "NutritionalStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "SM",  Name="Sev. Malnourished", SystemCode = "NutritionalStatusOptions" }
                                             };

            return GetNutritionalStatusOptions;

        }

        public IEnumerable<SystemCodeStringVm> GetClientStatusOptions()
        {
            List<SystemCodeStringVm> ClientStatusOptions = new List<SystemCodeStringVm> {
                                             new SystemCodeStringVm{ Id=0, Value = "0", Name="Select", SystemCode = "ClientStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "1", Name="Alive", SystemCode = "ClientStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "2", Name="Deceased", SystemCode = "ClientStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "3",  Name="Divorced", SystemCode = "ClientStatusOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "4",  Name="Graduated", SystemCode = "ClientStatusOptions" }
                                             };

            return ClientStatusOptions;

        }

        public IEnumerable<SystemCodeStringVm> GetSchoolGradeOptions()
        {
            List<SystemCodeStringVm> GetSchoolGradeOptions = new List<SystemCodeStringVm> {
                                             new SystemCodeStringVm{ Id=0, Value = "0", Name="Select", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "1", Name="1", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "2", Name="2", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "3",  Name="3", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "4",  Name="4", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "5",  Name="5", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "6",  Name="6", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "7",  Name="7", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "8",  Name="8", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "9",  Name="9", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "10",  Name="10", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "11",  Name="11", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "12",  Name="12", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "13",  Name="13", SystemCode = "SchoolGradeOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "14",  Name="14", SystemCode = "SchoolGradeOptions" }
                                             };

            return GetSchoolGradeOptions;

        }

        public IEnumerable<SystemCodeStringVm> GetPLWOptions()
        {
            List<SystemCodeStringVm> PLWOptions = new List<SystemCodeStringVm> {
                                             new SystemCodeStringVm{ Id=0, Value = "0", Name="Select", SystemCode = "PLWOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "P", Name="Pregnant", SystemCode = "PLWOptions" },
                                             new SystemCodeStringVm{ Id=0, Value = "L", Name="Lactating", SystemCode = "PLWOptions" }
                                             };

            return PLWOptions;
        }

        private readonly string path = HttpContext.Current.Server.MapPath("~/logs/");

        public void FileLog(string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path} log{todaydate}.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.WriteLine(data);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;
        }
        #endregion
    }
}