﻿using INSCTMIS.Api.Models;
using INSCTMIS.Api.ViewModels;
using INSCTMIS.Custom.Api.Data;
using INSCTMIS.Custom.Api.Helpers;
using INSCTMIS.Custom.Api.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace INSCTMIS.Api.Controllers
{
    public class HouseholdProfileController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Post HouseholdProfiles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/HouseholdProfile/PostHouseholdProfiles/")]
        [HttpPost]
        public IHttpActionResult PostHouseholdProfiles(TabletHouseholdProfileViewModel model)
        {
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            FileLog(JsonConvert.SerializeObject(model));
            ApiStatus apiFeedback;

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                FileLog(description);
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }

            try
            {
                var householdInfo = JsonConvert.DeserializeObject<HouseholdProfileVm>(model.HouseholdProfileInfo);
                var pdsHouseholdInfo = householdInfo.PDSHousehold;
                var plwHouseholdInfo = householdInfo.PLWHousehold;
                var cmcHouseholdInfo = householdInfo.CMCHousehold;

                var spName = "";
                var parameterNames = "";
                var parameterList = new List<ParameterEntity>();

                if (pdsHouseholdInfo != null)
                {
                    string rowXML = string.Empty;
                    string memberXML = string.Empty;
                    if(householdInfo.PDSHouseholdMembers != null)
                    {
                        var pdsHouseholdMembersInfo = householdInfo.PDSHouseholdMembers;
                        foreach (var item in pdsHouseholdMembersInfo)
                        {
                            rowXML += $"<row>"
                                + $"<ProfileDSDetailID>{item.ProfileDSDetailID}</ProfileDSDetailID>"
                                + $"<HouseHoldMemberName>{item.HouseHoldMemberName}</HouseHoldMemberName>"
                                + $"<IndividualID>{item.IndividualID}</IndividualID>"
                                + $"<Pregnant>{item.Pregnant}</Pregnant>"
                                + $"<Lactating>{item.Lactating}</Lactating>"
                                + $"<Age>{item.Age}</Age>"
                                + $"<Sex>{item.Sex}</Sex>"
                                + $"<PWL>{item.PWL}</PWL>"
                                + $"<Handicapped>{item.Handicapped}</Handicapped>"
                                + $"<ChronicallyIll>{item.ChronicallyIll}</ChronicallyIll>"
                                + $"<NutritionalStatus>{item.NutritionalStatus}</NutritionalStatus>"
                                + $"<childUnderTSForCMAM>{item.ChildUnderTSForCMAM}</childUnderTSForCMAM>"
                                + $"<EnrolledInSchool>{item.EnrolledInSchool}</EnrolledInSchool>"
                                + $"<Grade>{item.Grade}</Grade>"
                                + $"<SchoolName>{item.SchoolName}</SchoolName>"
                                + $"<ChildProtectionRisk>{item.ChildProtectionRisk}</ChildProtectionRisk>"
                                + $"</row>";
                        }
                        memberXML = $"<members>{rowXML}</members>";
                    }

                    spName = "InsertForm1ANew";
                    parameterNames = "@ProfileDSHeaderID,@KebeleID,@Gote,@Gare,@NameOfHouseHoldHead,@HouseHoldIDNumber,@Remarks,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@CBHIMembership,@CBHINumber,@memberXML,@CreatedBy,@CreatedOn";
                    parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileDSHeaderID",pdsHouseholdInfo.ProfileDSHeaderID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",pdsHouseholdInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Gote",pdsHouseholdInfo.Gote)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Gare",pdsHouseholdInfo.Gare)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NameOfHouseHoldHead",pdsHouseholdInfo.NameOfHouseHoldHead)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldIDNumber",pdsHouseholdInfo.HouseHoldIDNumber)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",pdsHouseholdInfo.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CollectionDate",pdsHouseholdInfo.CollectionDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",pdsHouseholdInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CCCCBSPCMember",pdsHouseholdInfo.CCCCBSPCMember)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CBHIMembership",pdsHouseholdInfo.CBHIMembership)},
                            new ParameterEntity { ParameterTuple = pdsHouseholdInfo.CBHINumber != null? new Tuple<string, object>("CBHINumber",pdsHouseholdInfo.CBHINumber) : new Tuple<string, object>("CBHINumber", DBNull.Value)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("memberXML",memberXML)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",pdsHouseholdInfo.CreatedBy)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedOn",pdsHouseholdInfo.CreatedOn)},
                        };

                    var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);

                    GenericService.AddOrUpdate(pdsHouseholdInfo.UserDevice);
                }

                if (plwHouseholdInfo != null)
                {
                    var plwHouseholdMembersInfo = householdInfo.PLWHouseholdMembers;
                    string rowXML = string.Empty;
                    string memberXML = string.Empty;
                    if(householdInfo.PLWHouseholdMembers != null)
                    {
                        foreach (var item in plwHouseholdMembersInfo)
                        {
                            rowXML += $"<row>"
                                + $"<ProfileTDSPLWDetailID>{item.ProfileDSDetailID}</ProfileTDSPLWDetailID>"
                                + $"<BabyDateOfBirth>{item.BabyDateOfBirth}</BabyDateOfBirth>"
                                + $"<BabyName>{item.BabyName}</BabyName>"
                                + $"<BabySex>{item.BabySex}</BabySex>"
                                + $"<NutritionalStatusInfant>{item.NutritionalStatusInfant}</NutritionalStatusInfant>"
                                + $"</row>";
                        }
                        memberXML = $"<members>{rowXML}</members>";

                    }

                    spName = "InsertForm1BNew";
                    parameterNames = "@ProfileTDSPLWID,@KebeleID,@Gote,@Gare,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@PLW,@NameOfPLW,@HouseHoldIDNumber,@MedicalRecordNumber,@PLWAge,@NutritionalStatusPLW,@StartDateTDS,@EndDateTDS,@Remarks,@ChildProtectionRisk,@CBHIMembership,@CBHINumber,@memberXML,@CreatedBy,@CreatedOn";
                    parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileTDSPLWID",plwHouseholdInfo.ProfileDSHeaderID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",plwHouseholdInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Gote",plwHouseholdInfo.Gote)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Gare",plwHouseholdInfo.Gare)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CollectionDate",plwHouseholdInfo.CollectionDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",plwHouseholdInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CCCCBSPCMember",plwHouseholdInfo.CCCCBSPCMember)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("PLW",plwHouseholdInfo.PLW)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NameOfPLW",plwHouseholdInfo.NameOfPLW)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldIDNumber",plwHouseholdInfo.HouseHoldIDNumber)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("MedicalRecordNumber",plwHouseholdInfo.MedicalRecordNumber)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("PLWAge",plwHouseholdInfo.PLWAge)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NutritionalStatusPLW",plwHouseholdInfo.NutritionalStatusPLW)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("StartDateTDS",plwHouseholdInfo.StartDateTDS)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("EndDateTDS",plwHouseholdInfo.EndDateTDS)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",plwHouseholdInfo.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildProtectionRisk",plwHouseholdInfo.ChildProtectionRisk)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CBHIMembership",plwHouseholdInfo.CBHIMembership)},
                            new ParameterEntity { ParameterTuple = plwHouseholdInfo.CBHINumber != null? new Tuple<string, object>("CBHINumber",plwHouseholdInfo.CBHINumber) : new Tuple<string, object>("CBHINumber", DBNull.Value)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("memberXML",memberXML)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",plwHouseholdInfo.CreatedBy)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedOn",plwHouseholdInfo.CreatedOn)},
                        };

                    var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);

                    GenericService.AddOrUpdate(plwHouseholdInfo.UserDevice);
                }

                if (cmcHouseholdInfo != null)
                {
                    spName = "InsertForm1CNew";
                    parameterNames = "@ProfileTDSCMCID, @KebeleID, @Gote, @Gare, @NameOfCareTaker, @CaretakerID, @HouseHoldIDNumber, @ChildID, @MalnourishedChildName, @MalnourishedChildSex, @ChildDateOfBirth, " + 
                                     "@DateTypeCertificate, @MalnourishmentDegree, @StartDateTDS, @NextCNStatusDate, @EndDateTDS, @CollectionDate, @SocialWorker, @CCCCBSPCMember, @CreatedBy, @CreatedOn, @Remarks," +
				                     "@CBHIMembership, @CBHINumber, @ChildProtectionRisk";
                    parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileTDSCMCID",cmcHouseholdInfo.ProfileDSHeaderID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",cmcHouseholdInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Gote",cmcHouseholdInfo.Gote)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Gare",cmcHouseholdInfo.Gare)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NameOfCareTaker",cmcHouseholdInfo.NameOfCareTaker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CaretakerID",cmcHouseholdInfo.CaretakerID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldIDNumber",cmcHouseholdInfo.HouseHoldIDNumber)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildID",cmcHouseholdInfo.ChildID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("MalnourishedChildName",cmcHouseholdInfo.MalnourishedChildName)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("MalnourishedChildSex",cmcHouseholdInfo.MalnourishedChildSex)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildDateOfBirth",cmcHouseholdInfo.ChildDateOfBirth)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("DateTypeCertificate",cmcHouseholdInfo.DateTypeCertificate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("MalnourishmentDegree",cmcHouseholdInfo.MalnourishmentDegree)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("StartDateTDS",cmcHouseholdInfo.StartDateTDS)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NextCNStatusDate",cmcHouseholdInfo.NextCNStatusDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("EndDateTDS",cmcHouseholdInfo.EndDateTDS)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CollectionDate",cmcHouseholdInfo.CollectionDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",cmcHouseholdInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CCCCBSPCMember",cmcHouseholdInfo.CCCCBSPCMember)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",cmcHouseholdInfo.CreatedBy)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedOn",cmcHouseholdInfo.CreatedOn)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",cmcHouseholdInfo.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CBHIMembership",cmcHouseholdInfo.CBHIMembership)},
                            new ParameterEntity { ParameterTuple = cmcHouseholdInfo.CBHINumber != null? new Tuple<string, object>("CBHINumber",cmcHouseholdInfo.CBHINumber): new Tuple<string, object>("CBHINumber", DBNull.Value)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildProtectionRisk",cmcHouseholdInfo.ChildProtectionRisk)},
                        };

                    var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);

                    GenericService.AddOrUpdate(cmcHouseholdInfo.UserDevice);
                }

                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "Success"
                };
                return Ok(apiFeedback);

            }
            catch (Exception e)
            {
                FileLog("\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n");
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
                return Ok(apiFeedback);
            }

            
        }

        #region Helpers
        private readonly string path = HttpContext.Current.Server.MapPath("~/logs/");

        public void FileLog(string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path} log{todaydate}.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.WriteLine(data);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;

            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        } 
        #endregion
    }
}