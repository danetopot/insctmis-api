﻿using INSCTMIS.Api.Models;
using INSCTMIS.Api.ViewModels;
using INSCTMIS.Custom.Api.Data;
using INSCTMIS.Custom.Api.Helpers;
using INSCTMIS.Custom.Api.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace INSCTMIS.Api.Controllers
{
    public class ComplianceController : ApiController
    {
        /// <summary>
        /// Get GetHouseholdsReadyForCompliance
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Compliance/GetHouseholdsReadyForCompliance/")]
        [HttpPost]
        public IHttpActionResult GetHouseholdsReadyForCompliance(ComplianceParameterVm model)
        {
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));

            var returnModel = new ComplianceVm();
            var spName = "";
            var parameterNames = "@KebeleID, @ReportingPeriodID";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",model.KebeleID)},
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",model.ReportingPeriodID)},
            };

            try
            {
                spName = "GetForm1AComplianceHouseholds";
                returnModel.TDSHouseholds = GenericService.GetManyBySp<ComplianceTDSVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetForm1AComplianceHouseholdMembers";
                returnModel.TDSHouseholdMembers = GenericService.GetManyBySp<ComplianceTDSMembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetForm1BComplianceHouseholds";
                returnModel.TDSPLWHouseholds = GenericService.GetManyBySp<ComplianceTDSPLWVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetForm1BComplianceHouseholdMembers";
                returnModel.TDSPLWHouseholdMembers = GenericService.GetManyBySp<ComplianceTDSPLWMembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetForm1CComplianceHouseholds";
                returnModel.TDSCMCHouseholds = GenericService.GetManyBySp<ComplianceTDSCMCVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetForm1CComplianceHouseholdMembers";
                returnModel.TDSCMCHouseholdMembers = GenericService.GetManyBySp<ComplianceTDSCMCMembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetChildProtectionHouseholds";
                returnModel.CPHouseholds = GenericService.GetManyBySp<ComplianceCPVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetChildProtectionHouseholdMembers";
                returnModel.CPHouseholdMembers = GenericService.GetManyBySp<ComplianceCPMembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetChildProtectionCasesforFollowUp";
                returnModel.CPCases = GenericService.GetManyBySp<ComplianceCPFollowUpVm>(spName, parameterNames, parameterList).ToList();

            }
            catch (Exception ex)
            {
                returnModel.Error = ex.Message;
                FileLog(":::::::::::::::::::::::::::: Error@api/SocialWorker/GetListingSettings : " + ex.Message + "\n\n" + ex.InnerException + " ::::::::::::::::::::::::::::");
                return Ok(returnModel);
            }

            FileLog(":::::::::::::::::::::::::::: Success@api/Compliance/GetHouseholdsReadyForCompliance ::::::::::::::::::::::::::::");
            return Ok(returnModel);
        }

        /// <summary>
        /// Get GetHouseholdsReadyForChildProtection
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Compliance/GetHouseholdsReadyForChildProtection/")]
        [HttpPost]
        public IHttpActionResult GetHouseholdsReadyForChildProtection(ComplianceParameterVm model)
        {
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));

            var returnModel = new ComplianceVm();
            var spName = "";
            var parameterNames = "@KebeleID, @ReportingPeriodID";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",model.KebeleID)},
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",model.ReportingPeriodID)},
            };

            try
            {
                spName = "GetChildProtectionHouseholds";
                returnModel.CPHouseholds = GenericService.GetManyBySp<ComplianceCPVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetChildProtectionHouseholdMembers";
                returnModel.CPHouseholdMembers = GenericService.GetManyBySp<ComplianceCPMembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetChildProtectionCasesforFollowUp";
                returnModel.CPCases = GenericService.GetManyBySp<ComplianceCPFollowUpVm>(spName, parameterNames, parameterList).ToList();

            }
            catch (Exception ex)
            {
                returnModel.Error = ex.Message;
                FileLog(":::::::::::::::::::::::::::: Error@api/Compliance/GetHouseholdsReadyForCompliance : " + ex.Message + "\n\n" + ex.InnerException + " ::::::::::::::::::::::::::::");
                return Ok(returnModel);
            }

            FileLog(":::::::::::::::::::::::::::: Success@api/Compliance/GetHouseholdsReadyForCompliance ::::::::::::::::::::::::::::");
            return Ok(returnModel);
        }

        // <summary>
        /// PostHouseholdComplianceData
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Compliance/PostHouseholdComplianceData/")]
        [HttpPost]
        public IHttpActionResult PostHouseholdsReadyForCompliance(TabletComplianceViewModel model)
        {
            ApiStatus apiFeedback;
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            var SerializeData = JsonConvert.SerializeObject(model);
            var PostedData = $" api/Compliance/PostHouseholdComplianceData/ \n " + SerializeData;
            FileLog(PostedData);

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                FileLog(description);
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                FileLog(description);
                return Ok(apiFeedback);
            }

            try
            {
                var complianceInfo = JsonConvert.DeserializeObject<ComplianceCaptureVm>(model.ComplianceInfo);
                var deviceInfo = complianceInfo.UserDevice;
                var spName = "";
                var parameterNames = "";
                var parameterList = new List<ParameterEntity>();
                
               
                switch (complianceInfo.HouseholdType)
                {
                    case "PDS":
                        spName = "InsertCapturedForm4ANew";
                        parameterNames = "@KebeleID,@ReportingPeriodID,@ProfileDSDetailID,@ServiceID,@HasComplied,@Remarks,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",complianceInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",complianceInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileDSDetailID",complianceInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ServiceID",complianceInfo.ServiceID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("HasComplied",complianceInfo.HasComplied)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",complianceInfo.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",complianceInfo.CreatedById)},
                        };
                        break;
                    case "PLW":
                        spName = "InsertCapturedForm4BNew";
                        parameterNames = "@KebeleID,@ReportingPeriodID,@ProfileTDSPLWID,@ServiceID,@HasComplied,@Remarks,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",complianceInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",complianceInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileTDSPLWID",complianceInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ServiceID",complianceInfo.ServiceID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("HasComplied",complianceInfo.HasComplied)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",complianceInfo.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",complianceInfo.CreatedById)},
                        };
                        break;
                    case "CMC":
                        spName = "InsertCapturedForm4CNew";
                        parameterNames = "@KebeleID,@ReportingPeriodID,@ProfileTDSPLWID,@ServiceID,@HasComplied,@Remarks,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",complianceInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",complianceInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileTDSPLWID",complianceInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ServiceID",complianceInfo.ServiceID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("HasComplied",complianceInfo.HasComplied)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",complianceInfo.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",complianceInfo.CreatedById)},
                        };
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }

                var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);
 
                GenericService.AddOrUpdate(deviceInfo);

                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "Success"
                };
                return Ok(apiFeedback);
            }
            catch (DbEntityValidationException e)
            {

                foreach (var eve in e.EntityValidationErrors)
                {
                    FileLog("Entity of type " + eve.Entry.Entity.GetType().Name +
                            " in state " + eve.Entry.State + " has the following validation errors:");

                    foreach (var ve in eve.ValidationErrors)
                    {
                        FileLog("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage);
                    }
                }

                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = $"{e.InnerException.ToString()}"
                };

                FileLog($"Error Uploading Compliance Data\n:- {e.ToString()}" + e.ToString());

                return Ok(apiFeedback);
            }
            catch (Exception e)
            {
                FileLog("\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n");
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = $"{e.InnerException.ToString()}"
                };

                FileLog($"Error Uploading Compliance Data\n:- {e.ToString()}" + e.ToString());

                return Ok(apiFeedback);
            }
        }

        // <summary>
        /// PostHouseholdChildProtectionData
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Compliance/PostHouseholdChildProtectionData/")]
        [HttpPost]
        public IHttpActionResult PostChildProtectionHousehold(TabletISNPViewModel model)
        {

            ApiStatus apiFeedback;
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            var SerializeData = JsonConvert.SerializeObject(model);
            var PostedData = $" api/Compliance/PostHouseholdChildProtectionData/ \n " + SerializeData;
            FileLog(PostedData);

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                FileLog(description);
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                FileLog(description);
                return Ok(apiFeedback);
            }

            try
            {
                var isnpInfo = JsonConvert.DeserializeObject<ISNPCaseVm>(model.ISNPInfo);
                var isnpCaseHeader = isnpInfo.CPCaseHeaderInfo;
                var isnpCaseDetails = isnpInfo.CPCaseDetailInfo;
                var isnpCaseVisitHeader = isnpInfo.CPCaseVisitHeaderInfo;
                var isnpCaseVisitDetails = isnpInfo.CPCaseVisitDetailInfo;
                var deviceInfo = isnpInfo.UserDevice;

                var spName = "";
                var parameterNames = "";
                var parameterList = new List<ParameterEntity>();

                if(isnpCaseHeader != null)
                {
                    string rowXML = string.Empty;
                    string memberXML = string.Empty;
                    foreach (var item in isnpCaseDetails)
                    {

                        rowXML += $"<row><RiskID>{item.RiskId}</RiskID>" + $"<ServiceID>{item.ServiceId}</ServiceID>" + $"<ProviderID>{item.ProviderId}</ProviderID></row>";

                    }
                    memberXML = $"<members>{rowXML}</members>";

                    spName = "InsertCapturedForm4DNew";
                    parameterNames = "@ChildProtectionId,@ChildDetailID,@CaseManagementReferral,@CaseStatusId,@ClientTypeID,@CollectionDate,@SocialWorker,@Remarks,@CreatedBy,@memberXML";
                    parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildProtectionId",isnpCaseHeader.ChildProtectionId)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildDetailID",isnpCaseHeader.ChildDetailID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CaseManagementReferral",isnpCaseHeader.CaseManagementReferral)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CaseStatusId", 1)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ClientTypeID",isnpCaseHeader.ClientTypeID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CollectionDate",isnpCaseHeader.CollectionDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",isnpCaseHeader.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("Remarks",isnpCaseHeader.Remarks)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",isnpCaseHeader.CreatedBy)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("memberXML", memberXML)},
                        };
                    var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);
                    GenericService.AddOrUpdate(deviceInfo);
                }

                if(isnpCaseVisitHeader != null)
                {
                    string rowXML = string.Empty;
                    string memberXML = string.Empty;
                    foreach (var item in isnpCaseVisitDetails)
                    {

                        rowXML += $"<row><ChildProtectionDetailID>{item.ChildProtectionDetailID}</ChildProtectionDetailID>" + $"<ServiceStatusID>{item.IsServiceAccessed}</ServiceStatusID>" + $"<ServiceAccessDate>{item.DateServiceAccessed}</ServiceAccessDate>" + $"<ServiceNotAccessedReasonID>{item.ServiceNotAccessedReason}</ServiceNotAccessedReasonID></row>";

                    }
                    memberXML = $"<members>{rowXML}</members>";

                    spName = "InsertForm4DFollowUpNew";
                    parameterNames = "@ChildProtectionHeaderID,@VisitDate,@SocialWorker,@CreatedBy,@memberXML";
                    parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ChildProtectionHeaderID",isnpCaseVisitHeader.ChildProtectionHeaderID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("VisitDate",isnpCaseVisitHeader.VisitDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",isnpCaseVisitHeader.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",isnpCaseVisitHeader.CreatedBy)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("memberXML", memberXML)},
                        };
                    var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);
                    GenericService.AddOrUpdate(deviceInfo);
                }


                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "Success"
                };
                return Ok(apiFeedback);
            }
            catch (DbEntityValidationException e)
            {

                foreach (var eve in e.EntityValidationErrors)
                {
                    FileLog("Entity of type " + eve.Entry.Entity.GetType().Name +
                            " in state " + eve.Entry.State + " has the following validation errors:");

                    foreach (var ve in eve.ValidationErrors)
                    {
                        FileLog("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage);
                    }
                }

                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = $"{e.InnerException.ToString()}"
                };

                return Ok(apiFeedback);
            }
            catch (Exception e)
            {
                FileLog("\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n");
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = $"{e.InnerException.ToString()}"
                };
                return Ok(apiFeedback);
            }
        }

        #region Helpers
        private readonly string path = HttpContext.Current.Server.MapPath("~/logs/");

        public void FileLog(string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path} log{todaydate}.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.WriteLine(data);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;

            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
        #endregion
    }
}