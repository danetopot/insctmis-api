﻿using INSCTMIS.Api.Models;
using INSCTMIS.Api.ViewModels;
using INSCTMIS.Custom.Api.Data;
using INSCTMIS.Custom.Api.Helpers;
using INSCTMIS.Custom.Api.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace INSCTMIS.Api.Controllers
{
    public class RetargetingController : ApiController
    {
        /// <summary>
        /// Get GetHouseholdsReadyForRetargeting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Retargeting/GetHouseholdsReadyForRetargeting/")]
        [HttpPost]
        public IHttpActionResult GetHouseholdsReadyForRetargeting(RetargetingParameterVm model)
        {
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            var returnModel = new RetargetVm();
            var spName = "";
            var parameterNames = "@KebeleID, @FiscalYear";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",model.KebeleID)},
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("FiscalYear",model.FiscalYear)},
            };

            try
            {
                spName = "GetRetargetingHouseholds";
                returnModel.RetargetingHouseholds = GenericService.GetManyBySp<RetargetingVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetRetargetingHouseholdMembers";
                returnModel.RetargetingHouseholdMembers = GenericService.GetManyBySp<RetargetingMembersVm>(spName, parameterNames, parameterList).ToList();

                returnModel.Message = $"Successfull";

                var data = JsonConvert.SerializeObject(returnModel);
                FileLog($":::::::::::::::::::::::::::: Success@api/Retargeting/GetHouseholdsReadyForRetargeting ::::::::::::::::::::::::::::\n\n" + data);
              
            }
            catch (Exception e)
            {
                returnModel.Message = e.Message;
                FileLog($":::::::::::::::::::::::::::: Error@api/Retargeting/GetHouseholdsReadyForRetargeting ::::::::::::::::::::::::::::\n\n" + e.Message);
            }
            return Ok(returnModel);
        }


        // <summary>
        /// PostHouseholdsReadyForRetargeting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Retargeting/PostHouseholdsReadyForRetargeting/")]
        [HttpPost]
        public IHttpActionResult PostHouseholdsReadyForRetargeting(TabletRetargetingViewModel model)
        {
            ApiStatus apiFeedback;
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            var SerializeData = JsonConvert.SerializeObject(model);
            var PostedData = $" api/Retargeting/PostHouseholdsReadyForRetargeting/ \n " + SerializeData;
            FileLog(PostedData);

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                FileLog(description);
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }

            try
            {
                var retargetingInfo = JsonConvert.DeserializeObject<RetargetingCaptureVm>(model.RetargetingInfo);
                var deviceInfo = retargetingInfo.UserDevice;
                var spName = "UpdateForm7New";
                var parameterNames = "@RetargetingHeaderID,@RetargetingDetailID,@SocialWorker,@CCCCBSPCMember,@Status,@Pregnant,@Lactating,@Handicapped,@ChronicallyIll,@NutritionalStatus,@ChildUnderTSForCMAM,@EnrolledInSchool,@SchoolName,@Grade,@IsUpdated,@DateUpdated,@CreatedBy";
                var parameterList = new List<ParameterEntity>();
                {
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("RetargetingHeaderID", retargetingInfo.RetargetingHeaderID) });
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("RetargetingDetailID", retargetingInfo.RetargetingDetailID)});
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SocialWorker", retargetingInfo.SocialWorker)});
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("CCCCBSPCMember", retargetingInfo.CCCMember)});
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("Status", retargetingInfo.Status)});
                        parameterList.Add(retargetingInfo.Pregnant != null ?  new ParameterEntity { ParameterTuple = new Tuple<string, object>("Pregnant", retargetingInfo.Pregnant)} : new ParameterEntity { ParameterTuple = new Tuple<string, object>("Pregnant", DBNull.Value) });
                        parameterList.Add(retargetingInfo.Lactating != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("Lactating", retargetingInfo.Lactating)} : new ParameterEntity { ParameterTuple = new Tuple<string, object>("Lactating", DBNull.Value) });
                        parameterList.Add(retargetingInfo.Handicapped != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("Handicapped", retargetingInfo.Handicapped) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("Handicapped", DBNull.Value) });
                        parameterList.Add(retargetingInfo.ChronicallyIll != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("ChronicallyIll", retargetingInfo.ChronicallyIll) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("ChronicallyIll", DBNull.Value) });
                        parameterList.Add(retargetingInfo.NutritionalStatus != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("NutritionalStatus", retargetingInfo.NutritionalStatus) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("NutritionalStatus", DBNull.Value) });
                        parameterList.Add(retargetingInfo.ChildUnderTSForCMAM != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("ChildUnderTSForCMAM", retargetingInfo.ChildUnderTSForCMAM) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("ChildUnderTSForCMAM", DBNull.Value) });
                        parameterList.Add(retargetingInfo.EnrolledInSchool != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("EnrolledInSchool", retargetingInfo.EnrolledInSchool) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("EnrolledInSchool", DBNull.Value) });
                        parameterList.Add(retargetingInfo.SchoolName != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SchoolName", retargetingInfo.SchoolName) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SchoolName", DBNull.Value) });
                        parameterList.Add(retargetingInfo.Grade != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("Grade", retargetingInfo.Grade) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("Grade", DBNull.Value) });
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("IsUpdated", retargetingInfo.IsUpdated)});
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("DateUpdated", retargetingInfo.DateUpdated)});
                        parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("CreatedBy", retargetingInfo.CreatedBy)});
                    };

                var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);

                GenericService.AddOrUpdate(deviceInfo);

                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "Success"
                };
                return Ok(apiFeedback);

            }
            catch (DbEntityValidationException e)
            {

                foreach (var eve in e.EntityValidationErrors)
                {
                    FileLog("Entity of type " + eve.Entry.Entity.GetType().Name +
                            " in state " + eve.Entry.State + " has the following validation errors:");

                    foreach (var ve in eve.ValidationErrors)
                    {
                        FileLog("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage);
                    }
                }

                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = $"{e.InnerException.ToString()}"
                };

                FileLog($"Error Uploading Retargeting Data\n:- {e.ToString()}" + e.ToString());

                return Ok(apiFeedback);

            }
            catch (Exception e)
            {
                FileLog("\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n");
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };

                FileLog($"Error Uploading Retargeting Data\n:- {e.ToString()}" + e.ToString());

                return Ok(apiFeedback);
            }


            apiFeedback = new ApiStatus
            {
                StatusId = 0,
                Description = "The Retargeting Details were Successfully Submitted"
            };

            return Ok(apiFeedback);
        }


        #region Helpers
        private readonly string path = HttpContext.Current.Server.MapPath("~/logs/");

        public void FileLog(string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path} log{todaydate}.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.WriteLine(data);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;

            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
        #endregion
    }
}