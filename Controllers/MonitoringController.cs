﻿using INSCTMIS.Api.Models;
using INSCTMIS.Api.ViewModels;
using INSCTMIS.Custom.Api.Data;
using INSCTMIS.Custom.Api.Helpers;
using INSCTMIS.Custom.Api.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace INSCTMIS.Api.Controllers
{
    public class MonitoringController : ApiController
    {
        /// <summary>
         /// Get GetHouseholdsReadyForMonitoring
         /// </summary>
         /// <param name="model"></param>
         /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Monitoring/GetHouseholdsReadyForMonitoring/")]
        [HttpPost]
        public IHttpActionResult GetHouseholdsReadyForMonitoring(MonitoringParameterVm model)
        {
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            var returnModel = new MonitoringVm();
            var spName = "";
            var parameterNames = "@KebeleID, @ReportingPeriodID";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",model.KebeleID)},
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",model.ReportingPeriodID)},
            };

            try
            {
                spName = "GetMonitoringTDS1Households";
                returnModel.TDS1Households = GenericService.GetManyBySp<Monitoring5A1Vm>(spName, parameterNames, parameterList).ToList();
                spName = "GetMonitoringTDS1HouseholdMembers";
                returnModel.TDS1HouseholdMembers = GenericService.GetManyBySp<Monitoring5A1MembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetMonitoringTDS2Households";
                returnModel.TDS2Households = GenericService.GetManyBySp<Monitoring5A2Vm>(spName, parameterNames, parameterList).ToList();
                spName = "GetMonitoringTDS2HouseholdMembers";
                returnModel.TDS2HouseholdMembers = GenericService.GetManyBySp<Monitoring5A2MembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetMonitoringPLWHouseholds";
                returnModel.PLWHouseholds = GenericService.GetManyBySp<Monitoring5BVm>(spName, parameterNames, parameterList).ToList();
                spName = "GetMonitoringPLWHouseholdMembers";
                returnModel.PLWHouseholdMembers = GenericService.GetManyBySp<Monitoring5BMembersVm>(spName, parameterNames, parameterList).ToList();

                spName = "GetMonitoringCMCHouseholds";
                returnModel.CMCHouseholds = GenericService.GetManyBySp<Monitoring5CVm>(spName, parameterNames, parameterList).ToList();
                spName = "GetMonitoringCMCHouseholdMembers";
                returnModel.CMCHouseholdMembers = GenericService.GetManyBySp<Monitoring5CMembersVm>(spName, parameterNames, parameterList).ToList();

                //spName = "GetMonitoringPLWHouseholds";
                //returnModel.PLWHouseholdDetails = GenericService.GetManyBySp<Monitoring5BVm>(spName, parameterNames, parameterList).ToList();

                //spName = "GetMonitoringCMCHouseholds";
                //returnModel.CMCHouseholdDetails = GenericService.GetManyBySp<Monitoring5CVm>(spName, parameterNames, parameterList).ToList();

                //spName = "GetMonitoringCPHouseholds";
                //returnModel.CPHouseholdDetails= GenericService.GetManyBySp<MonitoringCPHouseholdVm>(spName, parameterNames, parameterList).ToList();

                //spName = "GetMonitoringCPCaseDetails";
                //returnModel.CPCaseDetails = GenericService.GetManyBySp<MonitoringCPCasesVm>(spName, parameterNames, parameterList).ToList();

                var data = JsonConvert.SerializeObject(returnModel);
                returnModel.Message = $"Successfully Downloaded HouseholdsReadyForMonitoring";
                FileLog($":::::::::::::::::::::::::::: Success@api/Monitoring/GetHouseholdsReadyForMonitoring ::::::::::::::::::::::::::::\n\n" + data);
            }
            catch (Exception e)
            {
                returnModel.Message = $"Error :- " + e.Message;
                FileLog($":::::::::::::::::::::::::::: Error@api/Monitoring/GetHouseholdsReadyForMonitoring ::::::::::::::::::::::::::::\n\n" + e.Message);
            }
            return Ok(returnModel);
        }

        // <summary>
        /// PostHouseholdComplianceData
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [Route("api/Monitoring/PostHouseholdsReadyForMonitoring/")]
        [HttpPost]
        public IHttpActionResult PostHouseholdsReadyForMonitoring(TabletMonitoringViewModel model)
        {
            ApiStatus apiFeedback;
            var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            var SerializeData = JsonConvert.SerializeObject(model);
            var PostedData = $" api/Monitoring/PostHouseholdsReadyForMonitoring/ \n " + SerializeData;
            FileLog(PostedData);

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                FileLog(description);
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }

            try
            {
                var monitoringInfo = JsonConvert.DeserializeObject<MonitoringCaptureVm>(model.MonitoringInfo);
                var deviceInfo = monitoringInfo.UserDevice;
                var spName = "";
                var parameterNames = "";
                var parameterList = new List<ParameterEntity>();

                switch (monitoringInfo.HouseholdType)
                {
                    case "PDS1":
                        spName = "InsertCapturedForm5A1New";
                        parameterNames = "@ProfileDSDetailID,@KebeleID,@ReportingPeriodID,@CompletedDate,@NonComplianceReason,@ActionResponse,@SocialWorker,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileDSDetailID",monitoringInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",monitoringInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",monitoringInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CompletedDate",monitoringInfo.CompletedDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NonComplianceReason",monitoringInfo.NonComplianceReason)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ActionResponse",monitoringInfo.ActionResponse)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",monitoringInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",monitoringInfo.CreatedById)},
                        };
                        break;
                    case "PDS2":
                        spName = "InsertCapturedForm5A2New";
                        parameterNames = "@ProfileDSDetailID,@KebeleID,@ReportingPeriodID,@CompletedDate,@NonComplianceReason,@ActionResponse,@SocialWorker,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileDSDetailID",monitoringInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",monitoringInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",monitoringInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CompletedDate",monitoringInfo.CompletedDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NonComplianceReason",monitoringInfo.NonComplianceReason)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ActionResponse",monitoringInfo.ActionResponse)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",monitoringInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",monitoringInfo.CreatedById)},
                        };
                        break;
                    case "PLW":
                        spName = "InsertCapturedForm5BNew";
                        parameterNames = "@ProfileTDSPLWID,@KebeleID,@ReportingPeriodID,@CompletedDate,@NonComplianceReason,@ActionResponse,@SocialWorker,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileTDSPLWID",monitoringInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",monitoringInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",monitoringInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CompletedDate",monitoringInfo.CompletedDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NonComplianceReason",monitoringInfo.NonComplianceReason)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ActionResponse",monitoringInfo.ActionResponse)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",monitoringInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",monitoringInfo.CreatedById)},
                        };
                        break;
                    case "CMC":
                        spName = "InsertCapturedForm5CNew";
                        parameterNames = "@ProfileTDSCMCID,@KebeleID,@ReportingPeriodID,@CompletedDate,@NonComplianceReason,@ActionResponse,@SocialWorker,@CreatedBy";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProfileTDSCMCID",monitoringInfo.IndividualProfileID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("KebeleID",monitoringInfo.KebeleID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ReportingPeriodID",monitoringInfo.ReportingPeriodID)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CompletedDate",monitoringInfo.CompletedDate)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("NonComplianceReason",monitoringInfo.NonComplianceReason)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("ActionResponse",monitoringInfo.ActionResponse)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("SocialWorker",monitoringInfo.SocialWorker)},
                            new ParameterEntity { ParameterTuple =new Tuple<string, object>("CreatedBy",monitoringInfo.CreatedById)},
                        };
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }

                var returnModel = GenericService.GetManyBySp<RecordCountVm>(spName, parameterNames, parameterList);

                GenericService.AddOrUpdate(deviceInfo);

                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "Success"
                };
                return Ok(apiFeedback);

            }
            catch (DbEntityValidationException e)
            {

                foreach (var eve in e.EntityValidationErrors)
                {
                    FileLog("Entity of type " + eve.Entry.Entity.GetType().Name +
                            " in state " + eve.Entry.State + " has the following validation errors:");

                    foreach (var ve in eve.ValidationErrors)
                    {
                        FileLog("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage);
                    }
                }

                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = $"{e.InnerException.ToString()}"
                };

                FileLog($"Error Uploading Monitoring Data\n:- {e.ToString()}" + e.ToString());

                return Ok(apiFeedback);

            }
            catch (Exception e)
            {
                FileLog("\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n");
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = $"{e.InnerException.ToString()}"
                };

                FileLog($"Error Uploading Monitoring Data\n:- {e.ToString()}" + e.ToString());

                return Ok(apiFeedback);
            }

        }


        #region Helpers
        private readonly string path = HttpContext.Current.Server.MapPath("~/logs/");

        public void FileLog(string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path} log{todaydate}.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.WriteLine(data);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;
        }
        #endregion
    }
}