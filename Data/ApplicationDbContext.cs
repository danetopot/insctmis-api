using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using INSCTMIS.Api.Models;
using INSCTMIS.Custom.Api.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace INSCTMIS.Custom.Api.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base($"DefaultConnection")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 18000;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = true;
        }

        public DbSet<Users> User { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<Woreda> Woreda { get; set; }
        public DbSet<Kebele> Kebele { get; set; }
        public DbSet<ServiceProvider> ServiceProvider { get; set; }
        public DbSet<IntegratedServices> IntegratedService { get; set; }
        public DbSet<FiscalYears> FiscalYear { get; set; }
        public DbSet<ReportingPeriod> ReportingPeriod { get; set; }
        public DbSet<UserDevice> UserDevice { get; set; }
        public DbSet<SummaryForMIS> MonitoringService { get; set; }
        public DbSet<ProfileDSHeader> ProfileDSHeader { get; set; }
        public DbSet<ProfileDSDetail> ProfileDSDetail { get; set; }
        public DbSet<ProfileTDSPLW> ProfileTDSPLW { get; set; }
        public DbSet<ProfileTDSPLWDetail> ProfileTDSPLWDetail { get; set; }
        public DbSet<ProfileTDSCMC> ProfileTDSCMC { get; set; }
        public DbSet<RetargetingHeader> RetargetingHeader { get; set; }
        public DbSet<RetargetingDetail> RetargetingDetail { get; set; }
        public DbSet<ChildProtectionHeader> ChildProtectionHeader { get; set; }
        public DbSet<ChildProtectionDetail> ChildProtectionDetail { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}