﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(INSCTMIS.Custom.Api.Startup))]

namespace INSCTMIS.Custom.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
